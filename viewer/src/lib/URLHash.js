/*
 * Updates the URL hash with various viewer information.
 * This doesn't handle the "map" parameter, which is managed by MapLibre GL JS
 *
 * Based on https://github.com/maplibre/maplibre-gl-js/blob/main/src/ui/hash.ts
 *
 * @returns {URLHash} `this`
 */
class URLHash {
  constructor(viewer) {
    this._viewer = viewer;
    this._delay = null;
    window.addEventListener('hashchange', () => this._onHashChange(), false);
    ["view-rotated", "picture-loaded", "focus-changed"].forEach(e => this._viewer.on(e, () => this._updateHash()));
  }

  bindMapEvents() {
    this._viewer._map._map.on("moveend", () => this._updateHash());
  }

  getHashString() {
    let hash = '';
    let hashParts = {};

    const picMeta = this._viewer.getPictureMetadata();
    if (picMeta) {
        hashParts.pic = picMeta.id;
        hashParts.xyz = this._getXyzHashString();
    }

    if(this._viewer._map) {
      hashParts.map = this._getMapHashString();
      hashParts.focus = this._viewer.isMapWide() ? "map" : "pic";
    }

    Object.entries(hashParts)
    .sort((a,b) => a[0].localeCompare(b[0]))
    .forEach(entry => {
      let [ hashName, value ] = entry;
      let found = false;
      const parts = hash.split('&').map(part => {
        const key = part.split('=')[0];
        if (key === hashName) {
          found = true;
          return `${key}=${value}`;
        }
        return part;
      }).filter(a => a);
      if (!found) {
        parts.push(`${hashName}=${value}`);
      }
      hash = `${parts.join('&')}`;
    });

    return `#${hash}`;
  }

  _getCurrentHash() {
    // Get the current hash from location, stripped from its number sign
    const hash = window.location.hash.replace('#', '');

    // Split the parameter-styled hash into parts and find the value we need
    let keyvals = {};
    hash.split('&').map(
      part => part.split('=')
    ).forEach(part => {
      keyvals[part[0]] = part[1];
    });

    return keyvals;
  }

  _getMapHashString() {
    const center = this._viewer._map._map.getCenter(),
      zoom = Math.round(this._viewer._map._map.getZoom() * 100) / 100,
      // derived from equation: 512px * 2^z / 360 / 10^d < 0.5px
      precision = Math.ceil((zoom * Math.LN2 + Math.log(512 / 360 / 0.5)) / Math.LN10),
      m = Math.pow(10, precision),
      lng = Math.round(center.lng * m) / m,
      lat = Math.round(center.lat * m) / m,
      bearing = this._viewer._map._map.getBearing(),
      pitch = this._viewer._map._map.getPitch();
    let hash = `${zoom}/${lat}/${lng}`;

    if (bearing || pitch) hash += (`/${Math.round(bearing * 10) / 10}`);
    if (pitch) hash += (`/${Math.round(pitch)}`);

    return hash;
  }

  _getXyzHashString() {
    const xyz = this._viewer.getXYZ();
    const x = xyz.x.toFixed(2),
      y = xyz.y.toFixed(2),
      z = Math.round(xyz.z || 0);
    return `${x}/${y}/${z}`;
  }

  _onHashChange() {
    const vals = this._getCurrentHash();

    // Restore selected picture
    if(vals.pic) {
      this._viewer.goToPicture(vals.pic);
    }

    // Change focus
    if(vals.focus && ["map", "pic"].includes(vals.focus)) {
      this._viewer.setMapWide(vals.focus === "map");
    }

    // Change map position
    if(vals.map && this._viewer._map) {
      const mapOpts = this.getMapOptionsFromHashString(vals.map);
      if(mapOpts) {
        this._viewer._map._map.jumpTo(mapOpts);
      }
    }

    // Change xyz position
    if(vals.xyz) {
      const coords = this.getXyzOptionsFromHashString(vals.xyz);
      this._viewer.setXYZ(coords.x, coords.y, coords.z);
    }
  }

  getMapOptionsFromHashString(str) {
    const loc = str.split("/");
    if (loc.length >= 3 && !loc.some(v => isNaN(v))) {
      const res = {
        center: [+loc[2], +loc[1]],
        zoom: +loc[0],
        pitch: +(loc[4] || 0)
      };

      if(this._viewer._map) {
        res.bearing = this._viewer._map._map.dragRotate.isEnabled() && this._viewer._map._map.touchZoomRotate.isEnabled() ? +(loc[3] || 0) : this._viewer._map._map.getBearing();
      }

      return res;
    }
    else { return null; }
  }

  getXyzOptionsFromHashString(str) {
    const loc = str.split("/");
    if (loc.length === 3 && !loc.some(v => isNaN(v))) {
      const res = {
        x: +loc[0],
        y: +loc[1],
        z: +loc[2]
      };

      return res;
    }
    else { return null; }
  }

  _updateHash() {
    if(this._delay) {
      clearTimeout(this._delay);
      this._delay = null;
    }

    this._delay = setTimeout(() => {
      // Replace if already present, else append the updated hash string
      const location = window.location.href.replace(/(#.+)?$/, this.getHashString());
      try {
        window.history.replaceState(window.history.state, null, location);
      } catch (SecurityError) {
        // IE11 does not allow this if the page is within an iframe created
        // with iframe.contentWindow.document.write(...).
        // https://github.com/mapbox/mapbox-gl-js/issues/7410
      }
    }, 500);
  }

}

export default URLHash;
