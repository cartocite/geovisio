-- pictures-ts-index
-- depends: 20230113_01_0co97-rm-metadata-duplicates

CREATE INDEX pictures_ts_idx ON pictures(ts);
