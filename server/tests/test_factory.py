from src import create_app

def test_index(client):
	response = client.get('/')
	assert b'GeoVisio' in response.data
