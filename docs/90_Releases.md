# Make a release

GeoVisio uses [semantic versioning](https://semver.org/) for its release numbers.

Run these commands in order to issue a new release:

```bash
git checkout develop

cd server/
vim setup.cfg	# Change metadata.version

cd ../viewer/
vim package.json	# Change version
npm run doc

cd ../
vim CHANGELOG.md	# Replace unreleased to version number

git add *
git commit -m "Release x.x.x"
git tag -a x.x.x -m "Release x.x.x"
git push origin develop
git checkout main
git merge develop
git push origin main --tags
```
