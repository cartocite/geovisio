# Database setup

Geovisio relies on a [PostgreSQL](https://www.postgresql.org/) 10+ database with [PostGIS](https://postgis.net/) 3+ extension to run.

This documentation is important to read __if you're installing manually Geovisio__. If you want to deploy using __Docker__, you can go directly to [Docker install documentation](./10_Install_Docker.md).

First step is to install both PostgreSQL and PostGIS. Please refer to these software documentation to know more about their install.

If you're starting with a fresh PostgreSQL install (and not sure about what to do), you need to change these parameters in order to have GeoVisio working:

- Create a new database (with __UTF-8 encoding__) using this command:

```bash
sudo su - postgres -c "psql -c \"CREATE DATABASE geovisio ENCODING 'UTF-8' TEMPLATE template0\""
```

- Change the `listen_adresses` parameter in `postgresql.conf` to be `'*'`
- Add in `pg_hba.conf` file one of the following line (considering your username is `postgres` and database `geovisio`)

```
# If you run using Docker
host	geovisio	postgres	172.17.0.0/24	trust

# If you run directly on server
host	geovisio	postgres	127.0.0.1/32	trust
```

## Next step

Once your database is ready, you can install Geovisio, either with [Docker](./10_Install_Docker.md) or directly [on server](./10_Install_Classic.md).
