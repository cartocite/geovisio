# Available commands on classic deployment

Various operations can be run on GeoVisio server : reading your pictures and sequences, start API, migrate or clean-up database... This documentation cover available commands on a classic instance, if you're using Docker please [see this doc instead](./15_Running_Docker.md).

[[_TOC_]]

## Available commands

```
cleanup                Cleans up GeoVisio files and database.
db                     Commands to handle database operations
fill-mock-data         Creates mock sequences and images for testing...
redo-sequences         Re-processes already imported sequences.
set-sequences-heading  Changes pictures heading metadata.
```

To run Flask, you need to set the `FLASK_APP=src` environment variable:
```bash
FLASK_APP="src" flask <command>
```

Note that by default, thanks to [python-dotenv](https://github.com/theskumar/python-dotenv), the variable is defined in the `.flaskenv` file and automatically loaded. In that case, this variable doesn't need to be defined explicitly on command launch:

```bash
flask <command>
```

### Start API

This command starts the HTTP API to serve pictures & sequences to users.

```bash
# Development API
FLASK_ENV=development flask run

# Production HTTPS API
python3 -m waitress --port 5000 --url-scheme=https --call 'src:create_app'
```


### Process pictures & sequences

This command reads all available sequences and pictures in your filesystem (as pointed by `FS_URL` paramater) to import them in database and run blur algorithm (if enabled by `BLUR_STRATEGY` parameter).

```bash
flask process-sequences
```

### Database migration

As GeoVisio is actively developed, when updating from a previous version, some database migration could be necessary. If so, when starting GeoVisio, an error message will show up and warn about necessary migration. The following command has to be ran.

```bash
flask db upgrade
```

There might be no reason to do so, but if necessary, a migration rollback can also be done.

```bash
flask db rollback
```

A full database rollback (ie. removing all structures and data created by Geovisio) can also be done with this command.

```bash
flask db rollback --all
```

### Force pictures heading in sequence

Since version 1.4.0, you can import pictures without heading metadata. By default, heading is computed based on sequence movement path (looking in front), but you can edit manually after import using this command.

```bash
flask set-sequences-heading \
	--value <DEGREES_ROTATION_FROM_FORWARD> \
	--overwrite \
	<SEQUENCE_FOLDERNAME_1> <SEQUENCE_FOLDERNAME_2> ...
```

### Re-process some sequences

If you need to re-process one or several sequences (to re-do pictures blurring with another strategy for example, or after fixing metadata of some pictures), you can use the `redo-sequences` command followed by sequences folder names:

```bash
flask redo-sequences <SEQUENCE_FOLDERNAME_1> <SEQUENCE_FOLDERNAME_2> ...
```

### Generate mock pictures & sequences

If you want to feed Geovisio with mock data to test its performance, you can run this command:

```bash
BLUR_STRATEGY=DISABLE \
	flask fill-mock-data \
	--base-image-path=<path to the base image to duplicate>
```

This will create a number of sequences, and for each sequence duplicate the base image and update its exif metadata to change the position and its date.

### Clean-up

Eventually, if you want to clear database and delete derivate versions of pictures (it __doesn't__ delete original pictures), you can use the `cleanup` command:

```bash
flask cleanup
```

You can also run some partial cleaning with the same cleanup command and one of the following options:

```bash
flask cleanup \
    --database \ # Removes entries from database, but not picture derivates
    --cache \ # Only removes picture derivates, but not blur masks
    --blur-masks # Only removes blur masks
```


## Next step

Your server is up and running, you can [work with the API](./16_Using_API.md).
