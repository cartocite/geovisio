# GeoVisio Client quickstart

The GeoVisio viewer is a web JS library which displays pictures and sequences from a GeoVisio server, or any STAC API offering geolocated pictures.

__Contents__

[[_TOC_]]


## Install the viewer

### NPM

GeoVisio viewer is available on NPM as [geovisio](https://www.npmjs.com/package/geovisio) package.

```bash
npm install geovisio
```

If you want the latest version (corresponding to the `develop` git branch), you can use the `develop` NPM dist-tag:

```bash
npm install geovisio@develop
```

### Hosted version

You can rely on various providers offering hosted NPM packages, for example JSDelivr.

```html
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/geovisio@1.3.1/build/index.css" />
<script src="https://cdn.jsdelivr.net/npm/geovisio@1.3.1/build/index.js"></script>
```

Or use hosted version on our GeoVisio demo instance.

```html
<link rel="stylesheet" type="text/css" href="https://geovisio.fr/viewer/lib/index.css" />
<script src="https://geovisio.fr/viewer/lib/index.js"></script>
```

### Using this repository

You can install and use GeoVisio client based on code provided in this repository. All client-related code is in the `viewer/` directory.

This library relies on __Node.js 16__, which should be installed on your computer. Then, you can build the library using these commands:

```bash
cd viewer/
npm install
npm build
```

## Basic usage

A GeoVisio viewer can be initialized with a basic HTML `div`:

```html
<div id="viewer" style="width: 500px; height: 300px"></div>
```

Then, the div is populated with this JavaScript code:

```js
var instance = new GeoVisio.default(
	"viewer",  // Div ID
	"https://geovisio.fr/api/search",  // STAC API search endpoint
	{ map: true }  // Viewer options
);
```

All available options are listed in [this documentation](./21_Client_usage.md).


## Next steps

You may want to:

- Dive deep in [available JS methods](./21_Client_usage.md)
- Discover the available [URL settings](./22_Client_URL_settings.md)
